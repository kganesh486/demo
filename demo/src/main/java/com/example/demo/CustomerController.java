package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;
import java.util.Arrays;

@RestController
@RequestMapping("/")
public class CustomerController {
    @Autowired
    CustomerRepository repository;
    @GetMapping("/bulkcreate")
    public String bulkcreate(){
// save a single Customer
        repository.save(new Customer(1,"Rajesh", "Bhojwani"));
// save a list of Customers
        repository.saveAll(Arrays.asList(new Customer(2,"Salim", "Khan")
                , new Customer(3,"Rajesh", "Parihar")
                , new Customer(4,"Rahul", "Dravid")
                , new Customer(5,"Dharmendra", "Bhojwani")));
        return "Customers are created";
    }
    @PostMapping("/create")
    public String create(@RequestBody CustomerUI customer){
// save a single Customer
        repository.save(new Customer(customer.getId(),customer.getFirstName(), customer.getLastName()));
        return "Customer is created";
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<Customer> update(@PathVariable long id,@Valid @RequestBody CustomerUI customer)
    {
        customer.setId(id);
        return ResponseEntity.ok(repository.save(new Customer(customer.getId(),customer.getFirstName(), customer.getLastName())));
    }
    @GetMapping("hello")
    public String hello(@RequestParam(value="name",defaultValue="world")String name)
    {
        return String.format("Hello %s!",name);
    }
    @GetMapping("/findall")
    public List<CustomerUI> findAll(){
        List<Customer> customers = repository.findAll();
        List<CustomerUI> customerUI = new ArrayList<>();
        for (Customer customer : customers) {
            customerUI.add(new CustomerUI(customer.getFirstName(),customer.getLastName()));
        }
        return customerUI;
    }
    @RequestMapping("/search/{id}")
    public ResponseEntity<Customer> search(@PathVariable long id){
        Optional<Customer>customer=repository.findById(id);
        return ResponseEntity.ok(customer.get());
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> delete(@PathVariable long id)
    {
        repository.deleteById(id);
        return ResponseEntity.ok("Deleted");
    }
    @RequestMapping("/searchbyfirstname/{firstname}")
    public List<CustomerUI> fetchDataByFirstName(@PathVariable String firstname){
        List<Customer> customers = repository.findByFirstName(firstname);
        List<CustomerUI> customerUI = new ArrayList<>();
        for (Customer customer : customers) {
            customerUI.add(new CustomerUI(customer.getFirstName(),customer.getLastName()));
        }
        return customerUI;
    }
}
