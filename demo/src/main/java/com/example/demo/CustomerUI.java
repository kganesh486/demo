package com.example.demo;

public class CustomerUI {
    private String firstName,lastName;
    private long id;
    public CustomerUI(String firstName, String lastName) {
        this.firstName=firstName;
        this.lastName=lastName;
    }

    public String getFirstName() {
        return  firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public long getId()
    {
        return id;
    }
    public void setId(long id)
    {
        this.id=id;
    }
}
